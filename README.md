# ITmmunity

![Code Quality Score](https://api.codiga.io/project/18713/score/svg)
![Code Grade](https://api.codiga.io/project/18713/status/svg)
[![Codacy Badge](https://app.codacy.com/project/badge/Grade/3688460899c24743bc21c09cbcc5ce45)](https://www.codacy.com/gh/brainer3220/ITmmunity/dashboard?utm_source=github.com&utm_medium=referral&utm_content=brainer3220/ITmmunity&utm_campaign=Badge_Grade)

![Example](./Example.gif)

# Now Available
- [UnderKG](http://underkg.co.kr)
  - [News](http://underkg.co.kr/news)
- [Meeco](https://meeco.kr)
  - [News](https://meeco.kr/news)
